# How to Download #
The SDK is huge (about 4 GiB). Please **DO NOT CLONE** from Bitbucket directly (It will fail due to network flow limit).

We have provided a tar ball of the .git directory at:
http://pan.baidu.com/s/1eQjBGQI?qq-pf-to=pcqq.c2c

Please download it first, then uncompress it:

```
#!shell
mkdir -p ~/proj/firefly-rk3288
cd ~/proj/firefly-rk3288
tar xf /path/to/firefly-rk3288_sdk_git.tar.gz
git reset --hard
```

From now on, you can pull updates from Bitbucket:
```
#!shell
git pull origin master:master
```

Please visit the following website for more details of our Firefly-RK3288 devolopment board. Thank you.

http://www.t-firefly.com/html/en/home/

# 如何下载 #
SDK 非常巨大（约 4GiB），请**不要直接从 Bitbucket 下载 **仓库源码（由于流量限制的原因会出错）。

我们提供了 .git 目录的打包，放到云盘上以供下载：
http://pan.baidu.com/s/1eQjBGQI?qq-pf-to=pcqq.c2c

下载后，解压：

```
#!shell
mkdir -p ~/proj/firefly-rk3288
cd ~/proj/firefly-rk3288
tar xf /path/to/firefly-rk3288_sdk_git.tar.gz
git reset --hard
```

之后就可以从 Bitbucket 处获取更新的提交，保持同步:
```
#!shell
git pull origin master:master
```

想了解更多我们 Firefly-RK3288 开发板的详情，请浏览以下网址，谢谢！

http://www.t-firefly.com/html/en/home/