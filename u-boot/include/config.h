/* Automatically generated - do not edit */
#define CONFIG_SYS_ARCH  "arm"
#define CONFIG_SYS_CPU   "armv7"
#define CONFIG_SYS_BOARD "rk32xx"
#define CONFIG_SYS_VENDOR "rockchip"
#define CONFIG_SYS_SOC    "rk32xx"
#define CONFIG_BOARDDIR board/rockchip/rk32xx
#include <config_cmd_defaults.h>
#include <config_defaults.h>
#include <configs/rk32xx.h>
#include <asm/config.h>
#include <config_fallbacks.h>
#include <config_uncmd_spl.h>
